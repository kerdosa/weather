/*
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */

/**
 * Export weather service object.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
"use strict";


module.exports = require('./lib/weather-service');