weather-service
===========

Weather service wrapper.


## App Demo Screencast

[App Demo Screencast](https://www.youtube.com/watch?v=B39Vv7ZChrY)

## Installation
This module can be built as npm module or required as source code.

Install as a module if it's built as a module.

	$ npm install weather-service

Or copy `lib/weather-service.js` to the project source tree and require it.

## Usage

	var weatherService = require('weather-service');

The weather service exposes methods to get weather data given location or zip code. Each method except getWeatherForTravel requires either zipcode or location. The location can be city name like `San Francisco`. The zipcode parameter works only for US zip code.

The service methods return data through callback.

	weatherService.getWeather(params, function (err, data) {
		if (err) {
			// handle error
		} else {
			// data has weather info
		}
	});

## Sample Express App

The sample express app to demo weather service is available in `script/sample-app.js`.

To run the sample app,

	$ unzip weather-service.zip
	$ cd weather-service
	
	$ npm install

	$ node script/sample-app.js

The sample app exposes the following routes:

	/weather				: return weather data
		- parameters: zipcode, location, day
	/weather/wind			: return wind data
		- parameters: zipcode, location
	/weather/astronomy		: return sunrise/sunset info
		- parameters: zipcode, location
	/weather/atmosphere		: return atmosphere data
		- parameters: zipcode, location
	/weather/forecast		: return five days forecast
		- parameters: zipcode, location
	/weather/travel			: return weather along travel paths from origin to destination
		- parameters: origin, destination, weatherMode

Please use Postman with `script/postman.json` to test the sample app. Before using Postman, please create an environment in the Postman and declare `URL` variable as `http://localhost:3000`.


## Test

To run the test, type `npm test` in the project root folder.

	$ npm test


## Weather Service Methods

#### getWeather(params, callback)
The getWeather method returns weather data.

The params object accepts the following properties.

* zipcode: Only US zip code works. This has preference over location parameter.
* location: City name
* day: Specify the day weather data is returned. It can be today, tomorrow or day after tomorrow, or it can be number 0 through 4. By default day is 0, which means current weather is returned. When day is 1, tomorrow weather is returned.



```
	sample response:
	{
		"code": "29",
		"date": "Sat, 07 Mar 2015 9:52 pm MST",
		"temp": "36",
		"text": "Partly Cloudy"
	}
```

#### getWind(params, callback)
The getWind method returns current wind data.

The params object accepts the following properties.

* zipcode: Only US zip code works. This takes precedence over location parameter.
* location: City name


```
	sample response:
	{
		"chill": "58",
		"direction": "280",
		"speed": "7"
	}
```

#### getAstronomy(params, callback)
The getAstronomy method returns today's sunrise/sunset info.

The params object accepts the following properties.

* zipcode: Only US zip code works. This takes precedence over location parameter.
* location: City name

```
	sample response:
	{
		"sunrise": "6:31 am",
		"sunset": "6:08 pm"
	}
```

#### getAtmosphere(params, callback)
The getAtmosphere method returns current atmosphere info.

The params object accepts the following properties.

* zipcode: Only US zip code works. This takes precedence over location parameter.
* location: City name

```
	sample response:
	{
		"humidity": "81",
		"pressure": "30.05",
		"rising": "1",
		"visibility": "10"
	}
```

#### getForecast(params, callback)
The getForecast method returns five days forecast.

The params object accepts the following properties.

* zipcode: Only US zip code works. This takes precedence over location parameter.
* location: City name

```
	sample response:
	[{
		"code": "31",
		"date": "7 Mar 2015",
		"day": "Sat",
		"high": "71",
		"low": "50",
		"text": "Clear"
	},{
		"code": "34",
		"date": "8 Mar 2015",
		"day": "Sun",
		"high": "68",
		"low": "51",
		"text": "Mostly Sunny"
	},
		...
	]
```

#### getWeatherForTravel(params, callback)
The getWeatherForTravel method returns weather along travel paths from origin to destination. The travel method is car driving, the arrival time is the number of second that takes from origin to each path. The travel route is calculated by Google Directions API, this method return only paths that takes more than one hour along the travel path. Note that this method works only for US cities.

The params object accepts the following properties.

* origin: City name of travel origin.
* destination: City name of travel destination.
* weatherMode: It can be `current` or `arrival`, defaults to `current`.
	* current: returns current weather at each travel path.
	* arrival: returns the weather of arrival time at each travel path.

```
	sample response:
	[{
		"address": "Denver, CO 80202, USA",
		"arrival": 0,
		"weather": {
			"code": "33",
			"date": "Sat, 07 Mar 2015 8:57 pm MST",
			"temp": "37",
			"text": "Fair"
		}
	},
	{
		"address": "Big Springs, NE 69122, USA",
		"arrival": 9346,
		"weather": {
			"code": "33",
			"date": "Sat, 07 Mar 2015 9:14 pm MST",
			"temp": "31",
			"text": "Fair"
		}
	},
		...
	]
```


## Authors

Developed by [TopCoder](http://www.topcoder.com)


## License

Copyright (c) 2014 TopCoder, Inc. All rights reserved.