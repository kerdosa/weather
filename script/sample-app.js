/*
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */

/**
 * Test for weather-wrapper module.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
"use strict";


var express = require('express'),
  routes = require('./routes');


var app = express();
app.set('port', process.env.PORT || 3000);

// mount routes for weather service
app.use('/weather', routes);

var server = app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + server.address().port);
});

