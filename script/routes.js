/*
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */

/**
 * Sample express router to demo weather service.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
"use strict";

/**
 * This sample app is only for test and demo only. In production deploy,
 * route should validate input parameters.
 */

var express = require('express'),
  router = express.Router(),
  weatherService = require('../lib/weather-service');


function handleResponse(res, err, result) {
  if (err) {
    res.status(500).json({error: {
      name: err.name,
      message: err.message
    }});
  } else {
    res.status(200).json(result);
  }
}

// return weather
router.get('/', function(req, res, next) {
  weatherService.getWeather(req.query, function (err, result) {
    handleResponse(res, err, result);
  });
});

// return wind
router.get('/wind', function(req, res, next) {
  weatherService.getWind(req.query, function (err, result) {
    handleResponse(res, err, result);
  });
});

// return astronomy(sunrise, sunset)
router.get('/astronomy', function(req, res, next) {
  weatherService.getAstronomy(req.query, function (err, result) {
    handleResponse(res, err, result);
  });
});

// return atmosphere(humidity, pressure, rising, visibility)
router.get('/atmosphere', function(req, res, next) {
  weatherService.getAtmosphere(req.query, function (err, result) {
    handleResponse(res, err, result);
  });
});

// return forecast
router.get('/forecast', function(req, res, next) {
  weatherService.getForecast(req.query, function (err, result) {
    handleResponse(res, err, result);
  });
});

// return weathers along travel path
router.get('/travel', function(req, res, next) {
  weatherService.getWeatherForTravel(req.query, function (err, result) {
    handleResponse(res, err, result);
  });
});

module.exports = router;