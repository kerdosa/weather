/*
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */

/**
 * This contains weather service with Yahoo weather API(https://developer.yahoo.com/weather).
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
"use strict";


var YQL = require('yql'),
  googleMaps = require('googlemaps'),
  async = require('async'),
  _ = require('lodash'),
  debug = require('debug')('weather-service');


var TODAY = 'today';
var TOMORROW = 'tomorrow';
var DAY_AFTER_TOMORROW = 'day after tomorrow';

// getWeatherForTravel() returns current weather regardless arrival time.
var WEATHER_MODE_CURRENT = 'current';
// getWeatherForTravel() returns the weather at arrival time.
var WEATHER_MODE_ARRIVAL = 'arrival';

var DAYS = {
  'today': 0,
  'tomorrow': 1,
  'day after tomorrow': 2
};
var MAX_DAY = 4;

/**
 * Construct YQL statement to get weather info from Yahoo weather API.
 * @param {Object} params the input parameters
 * @returns {String} the YQL statement
 */
function _getWeatherQuery(params) {
  if (params.zipcode) {
    return "SELECT * FROM weather.forecast WHERE location = "+params.zipcode;
  } else if (params.location) {
    return "SELECT * FROM weather.forecast WHERE woeid in (select woeid from geo.places(1) where text = '"+params.location+"')";
  } else {
    throw new Error("Either zipcode or location is required");
  }
}

/**
 * Get weather info from Yahoo weather API.
 * @param {Object} params the input parameters
 * @param {Function(Error, Object)} callback the callback function
 */
function _getYahooWeather(params, callback) {
  if (!callback) {
    throw new Error('callback function is requried');
  }
  if (!_.isFunction(callback)) {
    throw new Error('callback must be function');
  }
  if (!params || (!params.zipcode && !params.location)) {
    return callback(new Error('Either zipcode or location is required'));
  }

  var query;
  try {
    query = new YQL(_getWeatherQuery(params));
  } catch (err) {
    return callback(err);
  }
  query.exec(function (err, data) {
    if (err) {
      callback(err);
    } else if (!data.query.results) {
      callback(new Error('Location is not found, invalid location '+params.location));
    } else if (!data.query.results.channel.item.condition) {
      callback(new Error('Location is not found, invalid zipcode '+params.zipcode));
    } else {
      callback(null, data);
    }
  });
}

/**
 * Parse day parameter into number.
 * @param {String} dayParam the day parameter
 * @returns {Number} the parsed day parameter in number
 */
function _parseDayParam(dayParam) {
  var day;
  if (dayParam) {
    var n = Number(dayParam);
    if (isNaN(n)) {
      n = DAYS[dayParam];
    }
    day = n;
  } else {
    day = 0;
  }
  return day;
}

/**
 * Get weather data given location or zipcode.
 * @param {Object} params the input parameters.
 *    - either zipcode or location is required
 *    - day can be today, tomorrow, day after tomorrow or number(0, 1, 2, 3, 4), optional, default to 0 which is today.
 * @param {Function(Error, Object)} callback the callback function
 */
function getWeather(params, callback) {
  _getYahooWeather(params, function (err, data) {
    if (err) {
      callback(err);
    } else { 
      // parse day parameter
      var day = _parseDayParam(params.day);
      if (day === 0) {  // today, return current weather
        callback(null, data.query.results.channel.item.condition);
      } else if (day > 0 && day <= MAX_DAY) {
        callback(null, data.query.results.channel.item.forecast[day]);
      } else if (day > MAX_DAY) {
        callback(new Error('Weather is available only up to four days'));
      } else {
        callback(new Error('day parameter only accept today, tomorrow, day after tomorrow, or 0 to '+MAX_DAY));
      }
    }
  });
}

/**
 * Get wind data given location or zipcode.
 * @param {Object} params the input parameters.
 *    - either zipcode or location is required
 * @param {Function(Error, Object)} callback the callback function
 */
function getWind(params, callback) {
  _getYahooWeather(params, function (err, data) {
    if (err) {
      callback(err);
    } else {
      callback(null, data.query.results.channel.wind);
    }
  });
}

/**
 * Get astronomy data(sunrise, sunset) given location or zipcode.
 * @param {Object} params the input parameters.
 *    - either zipcode or location is required
 * @param {Function(Error, Object)} callback the callback function
 */
function getAstronomy(params, callback) {
  _getYahooWeather(params, function (err, data) {
    if (err) {
      callback(err);
    } else { 
      callback(null, data.query.results.channel.astronomy);
    }
  });
}

/**
 * Get atmosphere data(humidity, pressure, rising, visibility) given location or zipcode.
 * @param {Object} params the input parameters.
 *    - either zipcode or location is required
 * @param {Function(Error, Object)} callback the callback function
 */
function getAtmosphere(params, callback) {
  _getYahooWeather(params, function (err, data) {
    if (err) {
      callback(err);
    } else { 
      callback(null, data.query.results.channel.atmosphere);
    }
  });
}

/**
 * Get 5 days forecast given location or zipcode.
 * @param {Object} params the input parameters.
 *    - either zipcode or location is required
 * @param {Function(Error, Object)} callback the callback function
 */
function getForecast(params, callback) {
  _getYahooWeather(params, function (err, data) {
    if (err) {
      callback(err);
    } else {
      callback(null, data.query.results.channel.item.forecast);
    }
  });
}

var ONE_HOUR = 60*60;
var ONE_DAY = 24*ONE_HOUR;

/**
 * Find address and zipcode from reverse geocode result.
 * @param {Object} reverseGeoResults the reverse geo results.
 * @param {Function(Error, Object)} callback the callback function
 */
function _findAddressZipcode(reverseGeoResults, callback) {
  var postalCodePlace = _.find(reverseGeoResults, function (place) {
    return place.types[0] === 'postal_code';
  });

  if (!postalCodePlace) {
    return callback(new Error('No portal_code is found'));
  }

  var zipcodeAddressComponent = _.find(postalCodePlace.address_components, function (address) {
    return address.types[0] === 'postal_code';
  });

  if (!zipcodeAddressComponent) {
    callback(new Error('postal_code address component is not found'));
  } else {
    callback(null, {address: postalCodePlace.formatted_address, zipcode: zipcodeAddressComponent.short_name});
  }
}

/**
 * Get weather along travel paths.
 * @param {Object} params the input parameters.
 *    - origin is required
 *    - destination is required
 *    - weatherMode: it can be current or arrival, default to current
 * @param {Function(Error, Object)} callback the callback function
 */
function getWeatherForTravel(params, callback) {
  if (!callback) {
    throw new Error('callback function is requried');
  }
  if (!_.isFunction(callback)) {
    throw new Error('callback must be function');
  }
  if (!params || !params.origin || !params.destination) {
    return callback(new Error('origin and destination are required'));
  }
  if (params.weatherMode) {
    if ((params.weatherMode !== WEATHER_MODE_CURRENT) && (params.weatherMode !== WEATHER_MODE_ARRIVAL)) {
      return callback(new Error('weatherMode supports only '+WEATHER_MODE_CURRENT+' or '+WEATHER_MODE_ARRIVAL));
    }
  }
  async.waterfall([
    function (cb) {  // get route by Google Map Directions API
      googleMaps.directions(params.origin, params.destination, function (err, data) {
        if (err) {
          cb(err);
        } else {
          var bigSteps = [];
          var arrivalTime = 0;  // unit is second
          var steps = data.routes[0].legs[0].steps; // use steps in the first route/leg
          async.eachSeries(steps, function (step, cb2) {
            if (bigSteps.length === 0) {  // add the starting location as first step
              bigSteps.push({end_location: step.start_location, arrival_time: 0});
            }
            arrivalTime += step.duration.value;
            if (step.duration.value > ONE_HOUR) {
              step.arrival_time = arrivalTime;
              bigSteps.push(step);
            }
            cb2();
          }, function (err) {
            if (bigSteps.length === 0) {  // use the last step if there is no big steps
              bigSteps.push(steps.pop());
            }
            debug('length of bigSteps: %d', bigSteps.length);
            cb(null, bigSteps);
          });
        }
      }, null, params.travelMode, null, false);
    },
    function (bigSteps, cb) {  // convert lat/lng to address/zipcode
      async.each(bigSteps, function (step, cb2) {
        var latlng = step.end_location.lat+','+step.end_location.lng;
        googleMaps.reverseGeocode(latlng, function (err, data) {
          if (err) {
            debug('error on reverseGeocode: %s', err.message);
            cb2();
          } else {
            _findAddressZipcode(data.results, function (err, result) {
              if (err) {
                debug('error on findAddressZipcode: %s', err.message);
              } else {
                // save address/zipcode to each step
                step.end_address = result.address;
                step.end_zipcode = result.zipcode;
                cb2();
              }
            });
          }
        });
      }, function (err) {
        cb(null, bigSteps);
      });

    },
    function (bigSteps, cb) {  // get weather at each step > 1hr
      async.each(bigSteps, function (step, cb2) {
        var previousZipcode;
        if (step.end_zipcode && step.end_zipcode !== previousZipcode) {
          previousZipcode = step.end_zipcode;
          var day = 0;  // by default get the current weather
          if (params.weatherMode && params.weatherMode === WEATHER_MODE_ARRIVAL) {
            day = Math.floor(step.arrival_time / ONE_DAY);
          }
          getWeather({zipcode: step.end_zipcode, day: day}, function (err, weather) {
            step.weather = weather;
            cb2();
          });
        } else {
          cb2();
        }
      }, function (err) {
        cb(null, bigSteps);
      });

    }], function (err, bigSteps) {
      if (err) {
        callback(err);
      } else {
        // return the address, arrival time and weather along the travel path
        var paths = [];
        _.each(bigSteps, function (step) {
          if (step.weather) {
            paths.push({
              address: step.end_address,
              arrival: step.arrival_time,
              weather: step.weather
            });
          }
        });
        callback(null, paths);
      }
    });
  
}


module.exports = {
  // constants
  MAX_DAY: MAX_DAY,
  TODAY: TODAY,
  TOMORROW: TOMORROW,
  DAY_AFTER_TOMORROW: DAY_AFTER_TOMORROW,
  WEATHER_MODE_CURRENT: WEATHER_MODE_CURRENT,
  WEATHER_MODE_ARRIVAL: WEATHER_MODE_ARRIVAL,

  // methods
  getWeather: getWeather,
  getWind: getWind,
  getAstronomy: getAstronomy,
  getAtmosphere: getAtmosphere,
  getForecast: getForecast,
  getWeatherForTravel: getWeatherForTravel
};