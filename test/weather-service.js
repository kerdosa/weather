/*
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */

/**
 * Test for weather-service.
 *
 * @author TCSASSEMBLER
 * @version 1.0
 */
"use strict";


var weatherService = require('../lib/weather-service'),
  _ = require('lodash'),
  assert = require("chai").assert;


describe('Weather Service', function () {
  this.timeout(3000);

  describe('Common required parameters zipcode and location', function () {
    it('should throw error if callback is not defined', function (done) {
      try {
        weatherService.getWeather({});
      } catch (e) {
        return done();
      }
      assert.fail();
    });

    it('should return error if neither zipcode or location is defined', function (done) {
      weatherService.getWeather({}, function (err, data) {
        assert.ok(err);
        done();
      });
    });

    it('should return error if neither zipcode or location is defined', function (done) {
      weatherService.getWeather({}, function (err, data) {
        assert.ok(err);
        done();
      });
    });

    it('should return error if zipcode is not valid', function (done) {
      weatherService.getWeather({zipcode: 11111}, function (err, data) {
        assert.ok(err);
        done();
      });
    });

    it('should return error if location is not valid city name', function (done) {
      weatherService.getWeather({location: 'San Applecisco'}, function (err, data) {
        assert.ok(err);
        done();
      });
    });

  });

  describe('getWeather() method', function () {
    var currentApiDate;
    /**
     * The checking date in the weather response fails right after midnight because Yahoo weather API still returns 
     * yesterday's date. To over come this issue, get the current date from Yahoo weather API response rather 
     * than the system date.
     */
    before(function (done) {
      weatherService.getWeather({zipcode: 95014}, function (err, data) {
        assert.ok(data);
        currentApiDate = new Date(data.date);
        done();
      });
    })
    it('should return error if day is more than MAX_DAY', function (done) {
      weatherService.getWeather({zipcode: 95014, day: weatherService.MAX_DAY+1}, function (err, data) {
        assert.ok(err);
        done();
      });
    });

    it('should return error if day is not today, or tomorrow or day after tomorrow', function (done) {
      weatherService.getWeather({zipcode: 95014, day: 'not-valid-day'}, function (err, data) {
        assert.ok(err);
        done();
      });
    });

    it('should return current weather if zipcode is valid and no day is defined', function (done) {
      weatherService.getWeather({zipcode: 95014}, function (err, data) {
        assert.ifError(err);
        assert.ok(data);
        assert.ok(data.temp);
        assert.equal(new Date(data.date).getDate(), currentApiDate.getDate());
        done();
      });
    });

    it('should return current weather if zipcode is valid and day is today', function (done) {
      weatherService.getWeather({zipcode: 95014, day: 'today'}, function (err, data) {
        assert.ifError(err);
        assert.ok(data);
        assert.ok(data.temp);
        done();
      });
    });

    it('should return tomorrow weather if zipcode is valid and day is tomorrow', function (done) {
      weatherService.getWeather({zipcode: 95014, day: 'tomorrow'}, function (err, data) {
        assert.ifError(err);
        assert.ok(data);
        assert.ok(data.high);
        assert.ok(data.low);
        assert.equal(new Date(data.date).getDate(), currentApiDate.getDate()+1);
        done();
      });
    });

    it('should return day-after-tomorrow weather if zipcode is valid and day is day after tomorrow', function (done) {
      weatherService.getWeather({zipcode: 95014, day: 'day after tomorrow'}, function (err, data) {
        assert.ifError(err);
        assert.ok(data);
        assert.ok(data.high);
        assert.ok(data.low);
        assert.equal(new Date(data.date).getDate(), currentApiDate.getDate()+2);
        done();
      });
    });

    it('should return day-after-tomorrow weather if zipcode is valid and day is 2', function (done) {
      weatherService.getWeather({zipcode: 95014, day: 2}, function (err, data) {
        assert.ifError(err);
        assert.ok(data);
        assert.ok(data.high);
        assert.ok(data.low);
        assert.equal(new Date(data.date).getDate(), currentApiDate.getDate()+2);
        done();
      });
    });

    it('should return current weather if location is valid', function (done) {
      weatherService.getWeather({location: 'San Francisco'}, function (err, data) {
        assert.ifError(err);
        assert.ok(data);
        assert.ok(data.temp);
        assert.equal(new Date(data.date).getDate(), currentApiDate.getDate());
        done();
      });
    });

  });

  describe('getWind() method', function () {
    it('should return current wind info if zipcode is valid', function (done) {
      weatherService.getWind({zipcode: 95014}, function (err, data) {
        assert.ifError(err);
        assert.ok(data);
        assert.ok(data.speed);
        done();
      });
    });

    it('should return current wind info if location is valid', function (done) {
      weatherService.getWind({location: 'San Francisco'}, function (err, data) {
        assert.ifError(err);
        assert.ok(data);
        assert.ok(data.speed);
        done();
      });
    });
  });

  describe('getAstronomy() method', function () {
    it('should return today sunrise/sunset if zipcode is valid', function (done) {
      weatherService.getAstronomy({zipcode: 95014}, function (err, data) {
        assert.ifError(err);
        assert.ok(data);
        assert.ok(data.sunrise);
        assert.ok(data.sunset);
        done();
      });
    });

    it('should return current sunrise/sunset if location is valid', function (done) {
      weatherService.getAstronomy({location: 'San Francisco'}, function (err, data) {
        assert.ifError(err);
        assert.ok(data);
        assert.ok(data.sunrise);
        assert.ok(data.sunset);
        done();
      });
    });
  });

  describe('getAtmosphere() method', function () {
    it('should return today atmosphere info if zipcode is valid', function (done) {
      weatherService.getAtmosphere({zipcode: 95014}, function (err, data) {
        assert.ifError(err);
        assert.ok(data);
        assert.ok(data.humidity);
        assert.ok(data.pressure);
        done();
      });
    });

    it('should return current atmosphere info if location is valid', function (done) {
      weatherService.getAtmosphere({location: 'San Francisco'}, function (err, data) {
        assert.ifError(err);
        assert.ok(data);
        assert.ok(data.humidity);
        assert.ok(data.pressure);
        done();
      });
    });
  });

  describe('getForecast() method', function () {
    it('should return 5 days forecast if zipcode is valid', function (done) {
      weatherService.getForecast({zipcode: 95014}, function (err, data) {
        assert.ifError(err);
        assert.ok(data);
        assert.equal(data instanceof Array, true);
        assert.equal(data.length, 5);
        done();
      });
    });

    it('should return 5 days forecast info if location is valid', function (done) {
      weatherService.getForecast({location: 'San Francisco'}, function (err, data) {
        assert.ifError(err);
        assert.ok(data);
        assert.equal(data instanceof Array, true);
        assert.equal(data.length, 5);
        done();
      });
    });
  });

  describe('getWeatherForTravel() method', function () {
    it('should return error if origin is not defined', function (done) {
      weatherService.getWeatherForTravel({destination: 'New York'}, function (err, data) {
        assert.ok(err);
        done();
      });
    });

    it('should return error if destination is not defined', function (done) {
      weatherService.getWeatherForTravel({origin: 'New York'}, function (err, data) {
        assert.ok(err);
        done();
      });
    });

    it('should return error if weatherMode is invalid', function (done) {
      var params = {origin: 'Denver', destination: 'New York', weatherMode: 'invalid-mode'};
      weatherService.getWeatherForTravel(params, function (err, data) {
        assert.ok(err);
        done();
      });
    });

    it('should return address, weather and arrival along travel paths if origin and destination are valid US city name', function (done) {
      this.timeout(5000);
      var params = {origin: 'Denver', destination: 'New York'};
      weatherService.getWeatherForTravel(params, function (err, data) {
        assert.ifError(err);
        assert.ok(data);
        var todayDate = new Date().getDate();
        assert.ok(_.isArray(data));
        assert.ok(data.length > 1);
        _.each(data, function (path) {
          assert.ok(path.address);
          assert.ok(_.isNumber(path.arrival));
          assert.ok(path.weather);
        });
        done();
      });
    });

  });
});

